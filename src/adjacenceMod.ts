namespace modAdjacence {

    export function runner(g: Graph) {
        // get adjacence and show it on top of the page 

        let divnode = document.createElement("div")
        let adj = g.getMatriceAdjacence()
        let tnode = document.createElement("table")
        tnode.id = "adjTable"
        let head = document.createElement("tr")
        let corner = document.createElement("th")
        corner.textContent = "/"
        head.appendChild(corner)
        for (let i = 0; i < g.vertices.length; ++i) {
            let th = document.createElement("th")
            th.textContent = g.vertices[i].nom
            head.appendChild(th)
        }
        tnode.appendChild(head)
        let arr: Array<Array<number>> = []
        for (let i = 0; i < g.vertices.length; ++i) {
            let ligne = document.createElement("tr")
            let nomLigne = document.createElement("td")
            nomLigne.textContent = g.vertices[i].nom
            if (fancyAdjacentColors) {
                ligne.style.backgroundColor = g.vertices[i].fillStyle
                ligne.style.color = g.vertices[i].strokeStyle
            }
            ligne.appendChild(nomLigne)
            for (let j = 0; j < g.vertices.length; ++j) {
                let td = document.createElement("td")
                td.textContent = adj[i][j].toString()
                ligne.appendChild(td)
            }
            tnode.appendChild(ligne)
        }
        let pnode = document.createElement("p")
        pnode.textContent = "raw content :"
        let textnode = document.createElement("input")
        textnode.type = "text"
        console.log(adj)
        textnode.value = JSON.stringify(adj)
        textnode.style.width = "70%"
        textnode.style.height = "2em"
        pnode.appendChild(textnode)

        divnode.appendChild(tnode)
        divnode.appendChild(pnode)

        interfaceOutputHTML(divnode)

        //adjacenceInterface?.appendChild(tnode)

    }
}


addModRunner(modAdjacence.runner, "Matrice d'adjacence")