let shortcuts: Array<ShortcutHandler> = []

let shortcutMode: ShortcutHandler | undefined
let shortcutInterface = document.createElement("div")


class ShortcutHandler {

    ctrl: boolean
    shift: boolean
    alt: boolean
    key: string
    name: string
    button: HTMLButtonElement
    linkedButton: HTMLButtonElement | HTMLInputElement
    constructor(name: string, button: HTMLButtonElement | HTMLInputElement) {
        this.linkedButton = button
        this.ctrl = false
        this.shift = false
        this.alt = false
        this.button = document.createElement("button")
        this.key = ""
        this.button.innerText = name + " : " + this.key
        this.name = name
        this.button.addEventListener("click", () => {
            shortcutMode = this
            this.button.innerText = "waiting for input"
        })
    }
    getElement() {
        let el = document.createElement("p")
        el.appendChild(this.button)
        return el
    }
    changekey(k: string, ctrl: boolean, shift: boolean, alt: boolean) {
        this.key = k
        this.ctrl = ctrl
        this.shift = shift
        this.alt = alt
        this.button.innerText = this.name + " : " + this.key
    }
    testE(event: KeyboardEvent) {
        if (event.key === this.key && event.ctrlKey === this.ctrl && event.shiftKey === this.shift && event.altKey === this.alt) {
            var evObj = document.createEvent('Events');
            evObj.initEvent("click", true, false);
            this.linkedButton.dispatchEvent(evObj);
        }
    }
}
let storageShortcut = localStorage.getItem("shortcuts")
let memShortcuts: Array<ShortcutHandler> | undefined = undefined
if (storageShortcut) {
    let parsedShortcut = JSON.parse(storageShortcut)
    if (parsedShortcut && parsedShortcut.length > 5) {
        memShortcuts = <Array<ShortcutHandler>>parsedShortcut
    }

}
if (memShortcuts && memShortcuts.length > 5) {
    shortcuts = memShortcuts
}
else {
    shortcuts.push(new ShortcutHandler("new", <HTMLButtonElement>newButton))
    shortcuts.push(new ShortcutHandler("save", <HTMLButtonElement>saveGButton))
    shortcuts.push(new ShortcutHandler("move", <HTMLButtonElement>moveButton))
    shortcuts.push(new ShortcutHandler("Vertex", <HTMLButtonElement>placeVButton))
    shortcuts.push(new ShortcutHandler("Arc", <HTMLButtonElement>placeAButton))
    shortcuts.push(new ShortcutHandler("Delete", <HTMLButtonElement>deleteButton))
    shortcuts.push(new ShortcutHandler("Sticky", stickyCheck))
}


let shortcutsButton = document.getElementById("shortcutsButton")
for (let i = 0; i < shortcuts.length; ++i) {
    shortcutInterface.appendChild(shortcuts[i].getElement())
}



shortcutsButton?.addEventListener("click", () => {
    interfaceOutputHTML(shortcutInterface)

})

let hitbox = <HTMLElement>document.getElementById("cvHitbox")

hitbox.addEventListener("keyup", function (e) {

    for (let i = 0; i < shortcuts.length; ++i) {
        shortcuts[i].testE(e)
    }

})

shortcutInterface.addEventListener("keydown", function (e) {
    if (shortcutMode) {
        shortcutMode.changekey(e.key, e.ctrlKey, e.shiftKey, e.altKey)
    }
    localStorage.setItem("shortcuts", JSON.stringify(shortcuts))

    shortcutMode = undefined
})