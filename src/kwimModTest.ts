namespace kwimModTest {
    export function runner(g: Graph) {
        // Test windows
        let testwin = new KwimMod.Window('wintest', 'Kwim Widget Tester', {x: 200, y: 8}, {x: 300, y: 400});

        let testprg: KwimMod.ProgressBar = new KwimMod.ProgressBar('testprg', {x: 8, y: 28}, {x: 284, y: 16}, 0.5);
        testwin.addChild(testprg);

        let textst: KwimMod.Text = new KwimMod.Text('textst', 'Hello, I\'m a child widget!', {x: 8, y: 8});
        testwin.addChild(textst);

        let testbtn: KwimMod.Button = new KwimMod.Button('testbtn', 'Button', {x: 8, y: 52}, {x: 92, y: 24});
        testwin.addChild(testbtn);
        
        testbtn.addClickHandler((target: KwimMod.Button) => {
            textst.setText('Button "' + target.getId() + '" clicked!');
        });

        let testsb: KwimMod.SeekBar = new KwimMod.SeekBar('testsb', {x: 116, y: 60}, {x: 100, y: 8});
        testwin.addChild(testsb);

        testsb.setProgress(0.5);
        testsb.addProgressHandler((target: KwimMod.SeekBar, normProgress: number) => {
            testprg.setNormalizedProgress(normProgress);
            textst.setText((normProgress * 100) + '%');
        });

        let testchk: KwimMod.CheckBox = new KwimMod.CheckBox('testchk', 'Checkbox', {x: 8, y: 84});
        testwin.addChild(testchk);

        testchk.addCheckedHandler((target: KwimMod.CheckBox, checked: boolean) => {
            textst.setText('Checkbox "' + target.getId() + '" is now ' + checked);
        });

        let testrad: KwimMod.RadioButton = new KwimMod.RadioButton('testrad', 'Radio button', {x: 120, y: 84});
        testwin.addChild(testrad);

        testrad.addCheckedHandler((target: KwimMod.RadioButton, checked: boolean) => {
            textst.setText('Radio button "' + target.getId() + '" is now ' + checked);
        });

        let testradg: KwimMod.RadioButtonGroup = new KwimMod.RadioButtonGroup('testradg', {x: 8, y: 108});
        testwin.addChild(testradg);

        testradg.addRadioButton('rad1', 'Radio 1');
        testradg.addRadioButton('rad2', 'Radio 2');
        testradg.addRadioButton('rad3', 'Radio 3');
        testradg.addChoiceHandler((target: KwimMod.RadioButton, checked: boolean) => {
            textst.setText('Radio button "' + target.getId() + '" is now ' + checked);
        });

        let testtxtbx: KwimMod.TextBox = new KwimMod.TextBox('testtxbx', {x: 8, y: 180}, {x: 284, y: 24});
        testwin.addChild(testtxtbx);

        testtxtbx.addTextEventHandler((target: KwimMod.TextBox, finished: boolean) => {
            textst.setText(finished + ' - ' + target.getText());
        });

        // testtxtbx.setInputPredicate((text: string) => {
        //     // Return true only if the entire string is numbers
        //     return new RegExp(/^[0-9]*$/g).test(text);
        // });

        let testspbx: KwimMod.SpinBox = new KwimMod.SpinBox('testspbx', {x: 8, y: 212}, {x: 284, y: 24});
        testwin.addChild(testspbx);

        testspbx.addValueHandler((target: KwimMod.SpinBox, value: number) => {
            textst.setText('SpinBox "' + target.getId() + '" now set to ' + value);
        });

        let testcolpick: KwimMod.ColorPicker = new KwimMod.ColorPicker('testcolpick', {x: 8, y: 244}, 64);
        testwin.addChild(testcolpick);

        testcolpick.addColorHandler((target: KwimMod.ColorPicker) => {
            const rgbColor: KwimMod.Vec3D = target.getColorAsRgb();
            textst.setText(target.getId() + ': <R: ' + Math.floor(rgbColor.x * 255) + ', G: ' + Math.floor(rgbColor.y * 255) + ', B: ' + Math.floor(rgbColor.z) * 255 + '>');
        });

        let testsw: KwimMod.Switch = new KwimMod.Switch('testsw', {x: 180, y: 296}, {x: 64, y: 24});
        testwin.addChild(testsw);

        testsw.addCheckedHandler((target: KwimMod.Switch, checked: boolean) => {
            textst.setText('Switch "' + target.getId() + '" is now ' + checked);
        });

        testwin.addCloseHandler((target: KwimMod.Window, retVal?: any) => {
            console.log('Window', target.getId(), 'closed with ret', retVal);
        });

        KwimMod.addWindow(new KwimMod.Window(Math.random().toString(), 'Other Window', {x: 600, y: 40}, {x: 256, y: 32}));
        KwimMod.addWindow(testwin);

        kwimATestWin = testwin;
    }
}

let kwimATestWin: KwimMod.Window;
addModRunner(kwimModTest.runner, "Kwim Tester")