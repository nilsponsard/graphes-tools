let fileInputG = document.getElementById("fileOpenGraph")
let saveGButton = document.getElementById("save")

class SaveSommet {
    id: number
    name: string
    position: pos
    fillStyle: string
    strokeStyle: string
    constructor(origin: Vertex) {
        this.id = origin.id
        this.name = origin.nom
        this.position = origin.position
        this.fillStyle = origin.fillStyle
        this.strokeStyle = origin.strokeStyle
    }

}
class SaveArc {
    verticesID: Array<number>
    fillStyle: string
    strokeStyle: string

    constructor(origin: Arc) {
        this.verticesID = [origin.vertices[0].id, origin.vertices[1].id]
        this.fillStyle = origin.fillStyle
        this.strokeStyle = origin.strokeStyle
    }
}



class SaveGraph {
    vertices: Array<SaveSommet>
    arcs: Array<SaveArc>
    directed: boolean
    constructor(origin: Graph) {
        this.directed = origin.directed
        this.arcs = []
        this.vertices = []
        for (let i = 0; i < origin.vertices.length; ++i) {
            this.vertices.push(new SaveSommet(origin.vertices[i]))
        }
        for (let i = 0; i < origin.arcs.length; ++i) {
            this.arcs.push(new SaveArc(origin.arcs[i]))
        }
    }
}
function unpack(pack: SaveGraph): Graph {
    let out = new Graph(pack.directed)
    for (let i = 0; i < pack.vertices.length; ++i) {
        let curr = pack.vertices[i]
        let v = out.addVertex(curr.name, curr.position, curr.id)
        v.fillStyle = curr.fillStyle
        v.strokeStyle = curr.strokeStyle
    }
    for (let i = 0; i < pack.arcs.length; ++i) {
        let curr = pack.arcs[i]
        if (out.findVertexById(curr.verticesID[0]) && out.findVertexById(curr.verticesID[1])) {
            let v1 = <Vertex>out.findVertexById(curr.verticesID[0])
            let v2 = <Vertex>out.findVertexById(curr.verticesID[1])
            out.addArc(v1, v2)
        }
    }
    return out
}

function openSave(this: HTMLInputElement, e: Event) {
    if (this.files) {
        if (this.files.length > 0) {
            let file = this.files[0]
            if (file) {
                let reader = new FileReader()
                reader.addEventListener("load", (e) => {
                    if (e.target?.result) {
                        let result = e.target.result.toString()
                        ipmortSave(result)
                    }
                })
                reader.readAsText(file)
            }
        }
    }
}
function ipmortSave(saveTxt: string) {
    let parsed = JSON.parse(saveTxt)
    // test if it's a SaveGraphe
    if (parsed.vertices && parsed.arcs && (parsed.directed === false || parsed.directed === true)) {
        if (parsed.vertices.length > 0) {
            if (parsed.vertices[0].id && parsed.vertices[0].name && parsed.vertices[0].position) {
                let obj = <SaveGraph>parsed
                try {

                    let unpacked = unpack(obj)
                    console.log(unpacked)
                    g = unpacked
                    refresh()
                    creationInterface?.classList.add("hidden")
                } catch (e) {
                    console.error("erreur lors de l'ouverture de la sauvegarde :")
                    console.error(e)
                }
            }
        }
    }
}


fileInputG?.addEventListener("change", openSave)
saveGButton?.addEventListener("click", (e) => {
    let objsave = new SaveGraph(g)
    console.log(objsave)
    let out = JSON.stringify(objsave)
    let file = new Blob([out], { type: "application/javascript" })
    // window.navigator.msSaveOrOpenBlob(file, "script.js");
    let a = document.createElement("a")
    let url = URL.createObjectURL(file)
    a.href = url
    a.style.display = "none"
    a.download = "graph.json"
    document.body.appendChild(a)
    a.click()
    setTimeout(function () {
        document.body.removeChild(a)
        window.URL.revokeObjectURL(url)
    }, 0)
})