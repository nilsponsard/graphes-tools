let scriptOpenButton = document.getElementById("scriptButton");
let scriptForm: HTMLFormElement = document.createElement("form");
let scriptTextArea = document.createElement("textarea");
let scriptRunButton = document.createElement("button");
let scriptSaveButton = document.createElement("button");

let scriptFileInputButton = document.createElement("input");
let scriptFileLabel = document.createElement("label");
scriptFileInputButton.id = "scriptFileInputButton";
scriptRunButton.innerText = "Run";
scriptSaveButton.innerText = "Save";
scriptFileLabel.htmlFor = "scriptFileInputButton";
scriptForm.appendChild(scriptRunButton);
scriptForm.appendChild(scriptSaveButton);
scriptFileLabel.innerHTML = "<br>Ouvrir un script : ";
scriptForm.appendChild(scriptFileLabel);
scriptFileInputButton.type = "file";
scriptForm.appendChild(scriptFileInputButton);
scriptTextArea.style.width = "100%";
scriptTextArea.style.height = "10rem";
scriptRunButton.type = "button";
scriptSaveButton.type = "button";
scriptForm.appendChild(scriptTextArea);

let scriptContent: string = "";

function importScript(this: HTMLInputElement, ev: Event) {
    if (this.files) {
        if (this.files.length > 0) {
            let file = this.files[0];
            if (file) {
                let reader = new FileReader();
                reader.addEventListener("load", (e) => {
                    if (e.target?.result) {
                        scriptContent = e.target?.result.toString();
                        scriptTextArea.value = scriptContent;
                    }
                });
                reader.readAsText(file);
            }
        }
    }
}
function runScript() {
    scriptContent = scriptTextArea.value;
    try {
        eval(scriptContent);
    } catch (error) {
        console.log(`une erreur est revenue ${error}`);
    }
    refresh();

}
function saveScript() {
    scriptContent = scriptTextArea.value;
    let file = new Blob([scriptContent], { type: "application/javascript" });
    // window.navigator.msSaveOrOpenBlob(file, "script.js");
    let a = document.createElement("a");
    let url = URL.createObjectURL(file);
    a.href = url;
    a.style.display = "none";
    a.download = "script.js";
    document.body.appendChild(a);
    a.click();
    setTimeout(function () {
        document.body.removeChild(a);
        window.URL.revokeObjectURL(url);
    }, 0);
}



scriptSaveButton.addEventListener("click", saveScript);
scriptRunButton.addEventListener("click", runScript);



scriptFileInputButton.addEventListener("change", importScript);


scriptOpenButton?.addEventListener("click", () => {


    interfaceOutputHTML(scriptForm);
});