# graphs-tools

A web App used to draw Graphs and apply algorithms


## Moding
Mods can be made and imported in the Mods/Function menu.  
You can add a function to the Mods/Function menu and by providing a function and a name to the `addModRunner()` function. The given callback/runner takes as parameter the main Graph object, documentation concerning the Graph object can be found [here](https://nilsponsard.gitlab.io/graphes-tools/docs/classes/graph.html).  
A mod can also have access to the [interfaceOutputHTML](https://nilsponsard.gitlab.io/graphes-tools/docs/globals.html#interfaceoutputhtml) function to draw html on top of the canvas.  
The `refresh()` function can be used to manually refresh the Canvas.
