
// all html elements
let nomVertexInput = <HTMLInputElement>document.getElementById("vertexName")
let fillVertexInput = <HTMLInputElement>document.getElementById("vertexFill")
let strokeVertexInput = <HTMLInputElement>document.getElementById("vertexStroke")

let cancelCreateButton = document.getElementById("cancel-create")

let creationInterface = document.getElementById("interface-new")
let vertexInterface = document.getElementById("interface-vertex")
let cancelVertexButton = document.getElementById("cancelVertex")
let captureButton = document.getElementById("capture")
let validateVertexButton = document.getElementById("validateVertex")
let createButton = document.getElementById("create")
let newButton = document.getElementById("new")
let moveButton = document.getElementById("move")
let placeVButton = document.getElementById("placeV")
let placeAButton = document.getElementById("placeA")
let deleteButton = document.getElementById("delete")
let stickyCheck = <HTMLInputElement>document.getElementById("sticky-checkbox")


// canvas setup
let saveCV = <HTMLCanvasElement>document.getElementById("saveCV")
let saveCTX = <CanvasRenderingContext2D>saveCV.getContext("2d")
let cv = <HTMLCanvasElement>document.getElementById("cv")
let ctx = <CanvasRenderingContext2D>cv.getContext("2d")

/*
 modes :
 0: move/bouger
 1: add vertex
 2: add arc
 3: delete vertex/arc
*/


let alphabet = "ABCDEFGHIJKLMNOPQRSTUVXYZ" // used for vertices names

const stickyBlockSize = 30
let showGrid = false
let mode = 1
let nom = "A"

let fancyAdjacentColors = true

let grabbed: Vertex | null = null
let g = new Graph()
let vertexsArc: Array<Vertex> = []

function capture() {
    // take a screenshot of the graph
    let minX = cv.width
    let maxX = 0
    let minY = cv.height
    let maxY = 0
    // get the relevant region
    for (let i = 0; i < g.vertices.length; ++i) {
        if (g.vertices[i].position.x + vertexSize > maxX) {
            maxX = g.vertices[i].position.x + vertexSize
        }
        if (g.vertices[i].position.y + vertexSize > maxY) {
            maxY = g.vertices[i].position.y + vertexSize
        }
        if (g.vertices[i].position.x - vertexSize < minX) {
            minX = g.vertices[i].position.x - vertexSize
        }
        if (g.vertices[i].position.y - vertexSize < minY) {
            minY = g.vertices[i].position.y - vertexSize
        }
    }
    minX = Math.max(minX, 0)
    minY = Math.max(minY, 0)
    maxX = Math.min(maxX, cv.width)
    maxY = Math.min(maxY, cv.height)

    let width = maxX - minX
    saveCV.width = width
    let height = maxY - minY
    saveCV.height = height
    // copy the region
    saveCTX.drawImage(cv, minX, minY, width, height, 0, 0, width, height)
    let url = saveCV.toDataURL("image/png")
    let a = document.createElement("a")
    a.href = url
    a.style.display = "none"
    a.download = "graph.png"
    document.body.appendChild(a)
    a.click()
    setTimeout(function () {
        document.body.removeChild(a)
        window.URL.revokeObjectURL(url)
    }, 0)
}
captureButton?.addEventListener("click", capture)


document.getElementById("barreOutil")?.addEventListener("click", refresh) // refresh on all actions

moveButton?.addEventListener("click", function () {
    mode = 0
})
placeVButton?.addEventListener("click", function () {
    mode = 1
})
placeAButton?.addEventListener("click", function () {
    mode = 2
})
deleteButton?.addEventListener("click", function () {
    mode = 3
})

stickyCheck.addEventListener("change", refresh)







newButton?.addEventListener("click", () => {

    creationInterface?.classList.remove("hidden")

})
createButton?.addEventListener("click", () => {
    // create a new graph
    creationInterface?.classList.add("hidden")
    let directed = <HTMLInputElement>document.getElementById("orienté")
    if (directed.checked) {
        g = new Graph(true)
    }
    else {
        g = new Graph(false)
    }
    nom = "A"
    refresh()
})
cancelCreateButton?.addEventListener("click", () => {
    creationInterface?.classList.add("hidden")
})

validateVertexButton?.addEventListener("click", () => {
    let nom = nomVertexInput.value
    let fill = fillVertexInput.value
    let stroke = strokeVertexInput.value
    if (grabbed != null) {
        if (nom != null && nom != "") {

            grabbed.nom = nom
        }
        if (fill != null && fill != "") {
            grabbed.fillStyle = fill
        }
        if (stroke != null && stroke != "") {
            grabbed.strokeStyle = stroke
        }

    }

    grabbed = null
    vertexInterface?.classList.add("hidden")
    refresh()

})

cancelVertexButton?.addEventListener("click", (e) => {
    vertexInterface?.classList.add("hidden")
    grabbed = null
})

cv.addEventListener("dblclick", (e) => {
    // edit on double click
    if (mode == 0) {
        let x = e.offsetX
        let y = e.offsetY
        grabbed = g.findVertexByPos({ x: x, y: y })
        if (grabbed != null) {
            vertexInterface?.classList.remove("hidden")
            nomVertexInput.value = grabbed.nom
            fillVertexInput.value = grabbed.fillStyle
            strokeVertexInput.value = grabbed.strokeStyle
        }
    }
})
cv.addEventListener("mousedown", (e) => {
    // handle all the canvas interactions
    let x = e.offsetX
    let y = e.offsetY
    if (stickyCheck.checked) {
        // round the values to make easier to align, used only for esthetics
        x = Math.round(x / stickyBlockSize) * stickyBlockSize
        y = Math.round(y / stickyBlockSize) * stickyBlockSize
    }
    if (grabbed == null) {
        switch (mode) {
            case 0:
                grabbed = g.findVertexByPos({ x: x, y: y })
                break
            case 1:
                let s = g.addVertex(nom, { x: x, y: y })
                let i = alphabet.lastIndexOf(nom[nom.length - 1])
                if (nom[nom.length - 1] === "Z") {
                    if (nom.length < 2)
                        nom = "A" + "A"
                    else {
                        if (nom[nom.length - 2] != "Z") {
                            nom = alphabet[alphabet.lastIndexOf(nom[nom.length - 2]) + 1] + "A"
                        }

                    }
                } else {
                    let splitted = nom.split('')
                    splitted[splitted.length - 1] = alphabet[i + 1]
                    nom = splitted.join('')
                }

                s.show(ctx)
                grabbed = s
                break
            case 2:
                if (vertexsArc.length < 2) {
                    let s = g.findVertexByPos({ x: x, y: y })
                    if (s != null) {
                        s.selected = true
                        vertexsArc.push(s)
                    }
                    if (vertexsArc.length >= 2) {
                        g.addArc(vertexsArc[0], vertexsArc[1])
                        vertexsArc[0].selected = false
                        vertexsArc[1].selected = false

                        vertexsArc = []
                        refresh()
                    }
                }

                break
            case 3:
                g.deleteAtPos({ x: x, y: y })
                break
        }
    } else {


        grabbed.selected = true
        grabbed.position.x = x
        grabbed.position.y = y
        grabbed.show(ctx)
    }

})
cv.addEventListener("mousemove", (e) => {
    let x = e.offsetX
    let y = e.offsetY
    if (stickyCheck.checked) {
        x = Math.round(x / stickyBlockSize) * stickyBlockSize
        y = Math.round(y / stickyBlockSize) * stickyBlockSize
    }
    if (grabbed != null) {
        grabbed.selected = true
        grabbed.position.x = x
        grabbed.position.y = y
        refresh()
        grabbed.show(ctx)
    }
})
cv.addEventListener("mouseup", (e) => {
    if (grabbed != null) {
        grabbed.selected = false
    }
    refresh()
    grabbed = null
    moveButton?.focus()


})






function drawStickyGrid(ctx: CanvasRenderingContext2D) {
    ctx.lineWidth = 1
    ctx.fillStyle = "#272727"
    ctx.strokeStyle = "#272727"
    ctx.beginPath()
    for (let x = 1; x < cv.width; x += stickyBlockSize) {
        ctx.moveTo(x, 0)
        ctx.lineTo(x, cv.height)
    }
    for (let y = 1; y < cv.width; y += stickyBlockSize) {
        ctx.moveTo(0, y)
        ctx.lineTo(cv.width, y)
    }

    ctx.stroke()
    ctx.closePath()
}


g.show(ctx)

let refreshCallbacks: Array<(context: CanvasRenderingContext2D) => any> = []



function refresh() {

    cv.width = window.innerWidth
    cv.height = window.innerHeight - 105
    if (stickyCheck.checked && showGrid) {
        drawStickyGrid(ctx)
    }
    g.show(ctx)
    moveButton?.classList.remove("selected")
    placeVButton?.classList.remove("selected")
    placeAButton?.classList.remove("selected")
    deleteButton?.classList.remove("selected")


    switch (mode) {
        case 0:
            moveButton?.classList.add("selected")
            break
        case 1:
            placeVButton?.classList.add("selected")

            break
        case 2:
            placeAButton?.classList.add("selected")
            break
        case 3:
            deleteButton?.classList.add("selected")
            break

    }
    refreshCallbacks.forEach((r) => {
        r(ctx)
    })




}






window.addEventListener("resize", () => {
    refresh()
})

refresh()
cv.addEventListener("mousemove", (e) => {

})