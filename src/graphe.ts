/** 
 * 
 * Core of the App with essentials classes and functions
 * @packageDocumentation
*/




/**
 * Position interface, used to place points on the graph
 */
interface pos {
    x: number;
    y: number;
}
function distancePoint(p1: pos, p2: pos) {
    return Math.sqrt((p1.x - p2.x) ** 2 + (p1.y - p2.y) ** 2);
}



let vertexSize = 40;
let vertexStroke = "#666666";
let vertexFill = "#FFFFFF";
let arcStroke = "black";
let arcFill = "black";
let arcSize = 2;




let idVertex = 1;


function return0ifUndefined(a: number | undefined) {
    if (a == undefined)
        return 0;
    return a;
}

/*
If the key exists, add 1 to the corresponding value
if not, set the value to 1
*/
function addToMapKey(m: Map<any, number>, key: any) {
    let a = m.get(key);
    if (a == undefined)
        a = 0;
    m.set(key, a + 1);
}


class Vertex {
    id: number;
    nom: string;
    position: pos;
    arcs: Array<Arc> = [];
    fillStyle: string;
    strokeStyle: string;
    selected = false;

    constructor(nom: string, position = { x: 0, y: 0 }, id: number | undefined = undefined) {
        this.nom = nom;
        this.position = position;
        if (id == undefined) {
            this.id = idVertex;
        } else {
            this.id = id;
        }
        this.strokeStyle = vertexStroke;
        this.fillStyle = vertexFill;
        ++idVertex;
    }
    getOrdre() {
        return this.arcs.length;
    }
    getAdjacent(): Map<Vertex, number> { // return vertices on which this vertex is the predecessor
        let m: Map<Vertex, number> = new Map();

        for (let i = 0; i < this.arcs.length; ++i) {
            if (this.arcs[i].vertices[0] === this.arcs[i].vertices[1])
                addToMapKey(m, this.arcs[i].vertices[1]);
            else
                if (this.arcs[i].directed) {

                    if (this.arcs[i].vertices[1] !== this)
                        addToMapKey(m, this.arcs[i].vertices[1]);
                } else
                    if (this.arcs[i].vertices[1] === this)
                        addToMapKey(m, this.arcs[i].vertices[0]);
                    else
                        addToMapKey(m, this.arcs[i].vertices[1]);
        }
        return m;
    }
    show(ctx: CanvasRenderingContext2D) {
        /*
        show the vertex 
        pos is the center of the circle 
        */

        ctx.beginPath();
        ctx.lineWidth = 2;
        ctx.arc(this.position.x, this.position.y, vertexSize / 2, 0, 360);
        ctx.strokeStyle = this.strokeStyle;
        if (this.selected) {
            ctx.strokeStyle = "blue";
        }


        ctx.fillStyle = this.fillStyle;
        ctx.stroke();
        ctx.fill();
        ctx.closePath();
        ctx.fillStyle = this.strokeStyle;

        ctx.font = `${vertexSize / 2}px sans-serif`;
        ctx.fillText(this.nom, this.position.x - (vertexSize / 4 * (this.nom.length + 1)) / 3, this.position.y + vertexSize / 8);
    }
    clean(): Array<Arc> {
        /*
        delete all arcs containing this vertex
        */
        let a = this.arcs.slice();
        for (let i = 0; i < this.arcs.length; ++i) {
            this.arcs[i].clean();
        }


        return a;
    }
}

class Arc {
    vertices: Array<Vertex>;
    directed: boolean;
    fillStyle: string;
    strokeStyle: string;
    numero: number;
    constructor(s1: Vertex, s2: Vertex, directed = false, nb: number) {
        this.vertices = [s1, s2];
        this.directed = directed;
        this.fillStyle = arcFill;
        this.strokeStyle = arcStroke;
        this.numero = nb;
        s2.arcs.push(this);
        s1.arcs.push(this);

    }
    show(ctx: CanvasRenderingContext2D) {

        ctx.beginPath();
        ctx.strokeStyle = this.strokeStyle;
        ctx.fillStyle = this.fillStyle;
        // simple line between the 2 vertices
        ctx.moveTo(this.vertices[0].position.x, this.vertices[0].position.y);
        ctx.lineTo(this.vertices[1].position.x, this.vertices[1].position.y);

        if (this.directed) {
            // draw an arrow if directed
            let d = 10;
            let angle = Math.PI / 8;
            let lineAngle = Math.atan2(this.vertices[1].position.y - this.vertices[0].position.y, this.vertices[1].position.x - this.vertices[0].position.x);
            let outerX = this.vertices[1].position.x + (vertexSize / 2) * Math.cos(lineAngle + Math.PI);
            let outerY = this.vertices[1].position.y + (vertexSize / 2) * Math.sin(lineAngle + Math.PI);
            let h = Math.abs(d / Math.cos(angle));
            let angle1 = lineAngle + Math.PI + angle;
            let topX = outerX + Math.cos(angle1) * h;
            let topY = outerY + Math.sin(angle1) * h;
            let angle2 = lineAngle + Math.PI - angle;
            let botX = outerX + Math.cos(angle2) * h;
            let botY = outerY + Math.sin(angle2) * h;
            ctx.moveTo(outerX, outerY);
            ctx.lineTo(topX, topY);
            ctx.lineTo(botX, botY);
            ctx.lineTo(outerX, outerY);
        }

        ctx.lineWidth = arcSize;
        ctx.fill();
        ctx.stroke();
        if (this.numero >= 1) {
            // print a number if there is more than 1 arc
            ctx.lineWidth = 1;
            let x = (this.vertices[0].position.x + this.vertices[1].position.x) / 2;
            let y = (this.vertices[0].position.y + this.vertices[1].position.y) / 2;
            let taille = 20;
            ctx.clearRect(x, y - taille / 2, taille, taille);
            ctx.font = `${taille / (1.5)}px monospace`;
            ctx.strokeText((this.numero + 1).toString(), x + taille / 4, y + taille / 8);
        }
        ctx.closePath();

    }
    clean(): Array<Vertex> {
        // make the arc leave all the vertices
        for (let i = 0; i < this.vertices.length; ++i) {
            this.vertices[i].arcs.splice(this.vertices[i].arcs.lastIndexOf(this), 1);
        }
        return this.vertices;
    }
}



// Main class, handle all the interactions
class Graph {
    vertices: Array<Vertex> = [];
    arcs: Array<Arc> = [];
    directed: boolean;
    constructor(directed = false) {
        this.directed = directed;
    }
    show(ctx: CanvasRenderingContext2D) {
        // draw everything contained 
        for (let i = 0; i < this.arcs.length; ++i)
            this.arcs[i].show(ctx);
        for (let i = 0; i < this.vertices.length; ++i)
            this.vertices[i].show(ctx);
    }
    getOrdreVertex(s: Vertex) {
        return s.getOrdre();
    }
    getOrdre() {
        return this.vertices.length;
    }
    getMatriceAdjacence() {
        let result: Array<Array<number>> = [];
        for (let i = 0; i < this.vertices.length; ++i) {
            let adj = this.vertices[i].getAdjacent();
            let line: Array<number> = [];
            for (let j = 0; j < this.vertices.length; ++j) {
                line.push(return0ifUndefined(adj.get(this.vertices[j])));
            }
            result.push(line);
        }
        return result;
    }
    getTaille() {
        return this.arcs.length;
    }
    addVertex(name: string, position: pos, id: number | undefined = undefined,) {
        let s = new Vertex(name, position, id);
        this.vertices.push(s);
        return s;
    }

    addArc(s1: Vertex, s2: Vertex): number {
        if (this.vertices.indexOf(s1) === -1 || this.vertices.indexOf(s2) === -1) {
            return 3; // a vertex is not in the graph !
        }
        if (s1 == s2) {
            return 2; // "loop" arcs not yet supported
        }
        let nb = 0;
        // check if there is other arcs
        for (let i = 0; i < this.arcs.length; ++i) {
            if (this.arcs[i].vertices.lastIndexOf(s1) != -1 && this.arcs[i].vertices.lastIndexOf(s2) != -1)
                ++nb;
        }

        this.arcs.push(new Arc(s1, s2, this.directed, nb));
        return 0;
    }

    findVertexByName(nom: string): Vertex | null {
        for (let i = 0; i < this.vertices.length; ++i) {
            if (nom === this.vertices[i].nom) {
                return this.vertices[i];
            }
        }
        return null;
    }
    findVertexById(id: number): Vertex | null {
        for (let i = 0; i < this.vertices.length; ++i) {
            if (id === this.vertices[i].id) {
                return this.vertices[i];
            }
        }
        return null;
    }

    findVertexByPos(position: pos): Vertex | null {
        let possibles = [];
        // get all vertices near the mouse 
        for (let i = 0; i < this.vertices.length; ++i) {
            if (distancePoint(position, this.vertices[i].position) < vertexSize / 2)
                possibles.push(this.vertices[i]);
        }

        // get the most relevant (whith the higher ID)
        let max = 0;
        let maxi = 0;
        for (let i = 0; i < possibles.length; ++i) {
            if (possibles[i].id > max) {
                max = possibles[i].id;
                maxi = i;
            }
        }
        if (max === 0) {
            return null;
        } else {
            return possibles[maxi];
        }

    }
    deleteAtPos(position: pos) {

        let s = this.findVertexByPos(position);
        if (s != null) {
            let a: Array<Arc> = [];
            let i = this.vertices.indexOf(s);
            if (i >= 0) {

                this.vertices.splice(i, 1);
                a = s.clean();
            }
            for (i = 0; i < a?.length; ++i) {
                this.arcs.splice(this.arcs.lastIndexOf(a[i]), 1);
            }

        }
        else { // hitbox with arcs
            let tolerance = 1;
            for (let i = 0; i < this.arcs.length; ++i) {
                let distanceIPoints = distancePoint(this.arcs[i].vertices[0].position, this.arcs[i].vertices[1].position);
                let distancePointSouris = distancePoint(this.arcs[i].vertices[0].position, position) + distancePoint(this.arcs[i].vertices[1].position, position);
                if (distanceIPoints - tolerance < distancePointSouris && distanceIPoints + tolerance > distancePointSouris) {
                    this.arcs[i].clean();
                    this.arcs.splice(i, 1);
                    return;
                }

            }

        }
    }

}