/**
 * show the form on screen and callback when a button is pressed
 */
function interfaceGetTextInput(form: HTMLFormElement, callback: (cancelled: boolean, form?: HTMLFormElement) => void, title: string = "") {
    interfaceInput?.classList.remove("hidden")
    if (title) {
        interfaceInputTitle.innerText = title

    } else {
        interfaceInputTitle.innerText = ""
    }
    interfaceInputContent.innerHTML = ""
    interfaceInputContent.appendChild(form)
    interfaceInputCancelButton.onclick = function () {
        interfaceInput?.classList.add("hidden")
        callback(true)
    }
    interfaceInputApplyButton.onclick = function () {
        interfaceInput?.classList.add("hidden")
        callback(true, form)
    }

}
/**
 * show via an interface an element
 * @param element html element to show
 * @param callback callback if needed, triggered when closing the message
 */
function interfaceOutputHTML(element: HTMLElement | Node, callback?: () => void) {
    interfaceOutputContent.innerHTML = ""
    interfaceOutput?.classList.remove("hidden")
    interfaceOutputContent?.appendChild(element)
    if (callback) {

        interfaceOutputBackButton.onclick = function () {
            interfaceOutput?.classList.add("hidden")
            callback()
        }
    } else {
        interfaceOutputBackButton.onclick = function () {
            interfaceOutput?.classList.add("hidden")
        }
    }
}